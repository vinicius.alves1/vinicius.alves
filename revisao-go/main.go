package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type User struct {
	Email string `json:email`
	Pass  string `json:pass`
}

var user User

const (
	messageHeader = `{
	"mensagem" : "header invalido"
}`

	messageBody = `{
	"mensagem" : "body invalido"
}`

	messageSuccess = `{
	"mensagem" : "Usuario registrado com sucesso"
}`

	messageLogin = `{
	"mensagem" : "Logado com sucesso"
}`
)

var registrados []User = []User{}

func main() {

	done := make(chan bool)
	go StartServer()
	log.Println("[INFO] Servidor no ar!")
	<-done

}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register", Register)
	r.HandleFunc("/login", Login)

	server := &http.Server{
		Addr:        "192.168.0.5:8082",
		IdleTimeout: duration,
		Handler:     r,
	}

	log.Print(server.ListenAndServe())
}

func Register(res http.ResponseWriter, req *http.Request) {

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &user)

	if VerificaMetodo(res, req) && VerificaHeader(res, req) {
		if user.Email == "" || user.Pass == "" {
			res.Header().Set("Status", "400")
			res.Write([]byte(messageBody))
		} else {
			registrados = append(registrados, user)
			res.Header().Set("Status", "200")
			res.Write([]byte(messageSuccess))
		}
	}
}

func VerificaMetodo(res http.ResponseWriter, req *http.Request) bool {
	metodo := req.Method

	if metodo != "POST" {
		res.Header().Set("Status", "405")
		return false
	}

	return true
}

func VerificaHeader(res http.ResponseWriter, req *http.Request) bool {
	headers := req.Header
	expectedHeader := headers["Content-Type"]

	if expectedHeader[0] != "application/json" {
		res.Header().Set("Status", "400")
		res.Write([]byte(messageHeader))
		return false
	}
	return true
}

func VerificaRegistrados(registrado []User, user User) bool {
	for i := range registrado {
		if registrado[i].Email == user.Email && registrado[i].Pass == user.Pass {
			return true
		}

	}
	return false
}

func Login(res http.ResponseWriter, req *http.Request) {

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &user)
	usuarioExiste := VerificaRegistrados(registrados, user)

	if VerificaMetodo(res, req) && VerificaHeader(res, req) {
		if user.Email == "" || user.Pass == "" {
			res.Header().Set("Status", "400")
			res.Write([]byte(messageBody))

		} else if !usuarioExiste {
			res.Header().Set("Status", "403")
		} else if usuarioExiste {
			res.Write([]byte(messageLogin))

		}

	}

}
